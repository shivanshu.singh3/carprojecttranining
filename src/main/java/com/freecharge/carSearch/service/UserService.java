package com.freecharge.carSearch.service;



import com.freecharge.carSearch.models.User;
import com.freecharge.carSearch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    // CREATE
    public User createUser(User user) {
        return userRepository.save(user);
    }

    // READ
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    // DELETE
    public void deleteUser(Short userId) {
        userRepository.deleteById(userId);
    }
}
