package com.freecharge.carSearch.service;


import com.freecharge.carSearch.models.Car;

import com.freecharge.carSearch.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CarServices {

    @Autowired
    CarRepository carRepository;

    // CREATE
    public Car createCar(Car car) {
        return carRepository.save(car);
    }

    // READ
    public List<Car> getCars() {
        return carRepository.findAll();
    }

    // DELETE
    public void deleteCar(String carName) {
        carRepository.deleteById(carName);
    }
}
