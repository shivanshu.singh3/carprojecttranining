package com.freecharge.carSearch.controller;


import com.freecharge.carSearch.models.Car;
import com.freecharge.carSearch.service.CarServices;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@RestController
@RequestMapping("/api/car")
public class CarController {
    @Autowired
    CarServices carServices;

    @RequestMapping(value="/cars", method=RequestMethod.POST)
    public Car createCar(@RequestBody Car car) {
        return carServices.createCar(car);
    }

    @RequestMapping(value="/cars", method=RequestMethod.GET)
    public List<Car> readCar() {
        return carServices.getCars();
    }

    @RequestMapping(value="/cars/{carName}", method=RequestMethod.DELETE)
    public void deleteEmployees(@PathVariable(value = "carName") String carName) {
        carServices.deleteCar(carName);
    }

}
