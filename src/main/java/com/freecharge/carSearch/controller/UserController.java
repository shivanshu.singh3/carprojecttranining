package com.freecharge.carSearch.controller;


import com.freecharge.carSearch.models.User;
import com.freecharge.carSearch.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value="/users", method= RequestMethod.POST)
    public User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @RequestMapping(value="/users", method=RequestMethod.GET)
    public List<User> readUser() {
        return userService.getUsers();
    }

    @RequestMapping(value="/users/{userId}", method=RequestMethod.DELETE)
    public void deleteEmployees(@PathVariable(value = "userId") short userId) {
        userService.deleteUser(userId);
    }

}

