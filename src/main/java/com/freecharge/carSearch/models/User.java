package com.freecharge.carSearch.models;

import javax.persistence.*;

@Entity
@Table(name = "user-data")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private short userId;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "USER_EMAIL")
    private String userEmail;
    @Column(name = "USER_CONTACTNO")
    private long userContactNo;
    @Column(name = "USER_MEMBERSHIP")
    private String userMembership;
    @Column(name = "USER_PASSWORD")
    private String userPassword;

    public User() {
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userContactNo=" + userContactNo +
                ", userMembership='" + userMembership + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }

    public User setUserPassword(String password) {
        this.userPassword = password;
        return this;
    }

    public short getUserId() {
        return userId;
    }

    public User setUserId(short userId) {
        this.userId = userId;
        return this;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public User setUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    public long getUserContactNo() {
        return userContactNo;
    }

    public User setUserContactNo(long userContactNo) {
        this.userContactNo = userContactNo;
        return this;
    }

    public String getUserMembership() {
        return userMembership;
    }

    public User setUserMembership(String userMembership) {
        this.userMembership = userMembership;
        return this;

    }
}
