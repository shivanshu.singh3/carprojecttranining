package com.freecharge.carSearch.models;


import javax.persistence.*;

@Entity
@Table(name = "car-data")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAR_NAME")
    private String carName;
    @Column(name = "CAR_PRICE")
    private int carPrice;
    @Column(name = "CAR_MAKER")
    private String makerName;
    @Column(name = "CAR_FUEL")
    private String fuelType;
    @Column(name = "CAR_RATING")
    private byte carRating;
    @Column(name = "CAR_DESC")
    private String carDescription;


    public Car() {

    }

    public int getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(int carPrice) {
        this.carPrice = carPrice;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getMakerName() {
        return makerName;
    }

    public void setMakerName(String makerName) {
        this.makerName = makerName;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public byte getCarRating() {
        return carRating;
    }

    public void setCarRating(byte carRating) {
        this.carRating = carRating;
    }

    public String getCarDescription() {
        return carDescription;
    }

    public void setCarDescription(String carDescription) {
        this.carDescription = carDescription;
    }

    @Override
    public String toString() {
        return "Car{" +
                ", carPrice=" + carPrice +
                ", carName='" + carName + '\'' +
                ", makerName='" + makerName + '\'' +
                ", fuelType='" + fuelType + '\'' +
                ", carRating=" + carRating +
                ", carDescription='" + carDescription + '\'' +
                '}';
    }
}
