package com.freecharge.carSearch.repository;

import com.freecharge.carSearch.models.User;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<User, Short> {

}
